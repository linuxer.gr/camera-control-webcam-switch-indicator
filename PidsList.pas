unit PidsList;

{$mode objfpc}{$H+}
{$warnings off}
{$hints off}

// Copyright © 2020 linuxer <linuxer@artixlinux.org>  <https://linuxer.gr>
// Created at 23th of January 2020, by Linuxer (https://gitlab.com/linuxergr), from scratch with Free Pascal
// Redesigned and further Developed at 28th of January 2020, by Initial developer
// to provide Camera and Mic status alone with On/Off and Mute/Unmute fuctions
// Developed further for intrusion feeling and logging at 2nd of February 2020, by Initial developer
// Developed for Blacklisting/Whitelisting functions for both camera & audio at 7th of February 2020, by Initial developer
// Finalized, except traslations at 15th of February 2020.
// Further Capabilities added alone with better Logging at 22nd of February 2020
// Essential Development Ended at 5th of March 2020. Minor Updates will follow, if needed
// Thanks to Ido Kanner idokan at@at gmail dot.dot com, libnotify has been added to the project, so to solve issues of notifiers and Qt5 on Linux
// https://forum.lazarus.freepascal.org/index.php/topic,52052.0.html
// Wayland and x11 operation confirmed too with the libnotify addition
// Further project development for the next major release 2.1.0, as from 3rd of Nov 2020 / ended at 6th of Nov 2020

interface

uses
  {$IFDEF UNIX}
          cthreads,
  {$ENDIF}
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ExtCtrls;

type

  { TForm9 }

  TForm9 = class(TForm)
    Bevel1      : TBevel;
    Button1     : TButton;
    Image1      : TImage;
    Label1      : TLabel;
    Label2      : TLabel;
    ScrollBar1  : TScrollBar;
    procedure Button1Click(Sender: TObject);
  private

  public

  end;

var
  Form9: TForm9;

implementation

{$R *.frm}

{ TForm9 }

procedure TForm9.Button1Click(Sender: TObject);
begin
     if fsModal in FFormState then
        ModalResult := mrClose
     else
         Form9.Hide;
end;


end.
