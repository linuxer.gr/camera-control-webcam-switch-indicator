# `Camera Control for Linux in System Tray (Notification Area)`

-  A small utility to switch your webcam on/off, microphone mute/unmute for Linux Desktops ([Qt5](https://gitlab.com/linuxer.gr/camera-control-webcam-switch-indicator/issues/5), [Gtk3 maybe in the future](https://gitlab.com/inuxer.gr/camera-control-webcam-switch-indicator/issues/4))

- This project replaces the [camera monitor](https://launchpad.net/cameramonitor), for having the full webcam status on tray and provides for both camera and microphone switch functions aditionally.

- In addition, it provides Security Control, by Logs, Popup notifiers and Shield Icon on tray for Malware or Intrusion Detection.

- Polkit integration prevents leakage of sensitive data, as from 2.2.0 release and on 

    ![Screenshot](https://imgur.com/9qNDcYz.png)
   
- so the CameraControl.sh, has been modified accordingly
     
    [![Screenshot](https://imgur.com/xea7CYZ.png)

- RPM is not valid yet. I do not have any feedback and testing from such distributions. 

- After editing, you may set it to autostart with your favourite Desktop, from your Home folder.


--------------------------------------------------------------------------------------------------------------------------------------------------------------------

### Some screenshots (but also see the latest [release videos](https://gitlab.com/psposito/camera-control-webcam-switch-indicator/-/blob/master/README.md#video-demos) below: 
 (Leftside of images is the System Blacklist/Whitelist added functions, Camera / Microphone Shield status tray icon, Microphone Staus Icon and the last is the Camera Control tray icon):

#### Note: Some of them belong to previous versions, but are more, or less, the same outputs, except that from 2.0.2 Release, are normal Windovw Forms, not messages.

- Microphone Unmuted, Camera On:

    ![Screenshot](https://imgur.com/OHXn1kQ.png)
    
- Capturing (Do not use Camera Off,if you want to force stop it any more)

    ![Screenshot](https://imgur.com/WTlgkrl.png)
    
- Stop Capturing (kill), best to be at System level

    ![Screenshot](https://imgur.com/eDfc0Ub.png)
    
    Process number information 
    
    ![Screenshot](https://imgur.com/YIu4SJy.png)
    
- Switch Camera to Off:
    
    ![Screenshot](https://imgur.com/ll6HWum.png) 

- Camera is Off for the User and Applications using the Webcam driver, Do not use to stop Capturing (can be killed) [#5](https://gitlab.com/psposito/camera-control-webcam-switch-indicator/issues/5):

    ![Screenshot](https://imgur.com/25SJxHl.png)
    
    [![](http://img.youtube.com/vi/ncFUDIMkdpw/0.jpg)](http://www.youtube.com/watch?v=ncFUDIMkdpw "Issue #5")
    
    [YouTube link](https://youtu.be/ncFUDIMkdpw)

- Switch Microphone to Mute:
    
    ![Screenshot](https://imgur.com/LVC4gfg.png)

- Camera Off, Microphone Muted:

    ![Screenshot](https://imgur.com/ld6BMkz.png)

- Sudo password input, normally entered once per application session, or if Password Reset has been executed:
    
    ![Screenshot](https://imgur.com/SkTEjal.png)   

- Shield event (for both microphone and camera):
    
    ![Screenshot](https://imgur.com/rB9HbTw.png)

- Shield event status (Microphone was hacked, switched status to Unmuted):

    ![Screenshot](https://imgur.com/2OqSpBi.png)
    
- Events Log menu:
    
    ![Screenshot](https://imgur.com/EPQFZii.png)

- Events Log List:
    
    ![Screenshot](https://imgur.com/zaRA2Uu.png)

- Reset Shield Status Icon:

    ![Screenshot](https://imgur.com/NyZRKMJ.png)

- System Menu (Blaclist/Whitelist Camera and/or Microphone), About and Exit functions: 

    ![Screenshot](https://imgur.com/XAAjsrS.png)
    
- Reboot Window: 

    ![Sreenshot](https://imgur.com/eGKNVKC.png)
    
- ### Note that before reboot, all Blacklist/Whitelist functions are being reverted (Undone), if Cancel button is pressed.

- About: 

    ![Screenshot](https://imgur.com/JQbK2EP.png)
    
    (click on the links to open)
    
- Forced to Reboot if Kernel module V4L2 produces 59 lines by switching the Camera, [from On/Off and from Off/On, when Hacking Events occur](https://gitlab.com/inuxer.gr/camera-control-webcam-switch-indicator/-/wikis/Project-History-and-other-Details#found-v4l2-limitations-forcing-for-reboot-when-v4l2-video-on-comes-to-number-60-it-is-mandatory-to-reboot-because-kernel-does-not-generate-more-devices-and-video-capture-is-not-possible)  

    ![Screenshot](https://imgur.com/bUjoU0j.png) 
    

### Video Demos: 
- #### The second major release 2.2.0 polkit and notify-osd in action

- [![Release 2.2.0 Demo](http://img.youtube.com/vi/_Z6MtPDJi9Y/0.jpg)](https://www.youtube.com/watch?v=_Z6MtPDJi9Y)

- #### The first major Release 2.0.2 Demo

- [![Release 2.0.2 Demo](http://img.youtube.com/vi/8b2H3lYPNc8/0.jpg)](http://www.youtube.com/watch?v=8b2H3lYPNc8)

- ### Release 2.0.1
  #### Demo
- [![Release 2.0-1 Demo](http://img.youtube.com/vi/V-VOqm7Cbi8/0.jpg)](http://www.youtube.com/watch?v=V-VOqm7Cbi8) 
 
  #### Further Information for the release
- [![Release 2.0-1 Info](http://img.youtube.com/vi/2F9eMjQEzGc/0.jpg)](http://www.youtube.com/watch?v=2F9eMjQEzGc)

- #### [Previous Videos at Youtube Playlist](https://www.youtube.com/playlist?list=PLAG2B-41QEHVhg2O8flo-gUIE1Mfg8iRt))

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
### Prerequisites 

#### For Desktops not based on Gtk, but on Qt i.e. KDE, LxQt etc, it may be neccessary to be installed, for proper appearance of icons, menus e.t.c., the following package: 

- ##### `qt5pas` (Arch, AUR), 
- ##### `libqt5pas-dev`, `libqt5pas1` (Debian/Ubuntu),
- ##### `libQt5Pas-devel`, `libQt5Pas` from [ecsos](http://download.opensuse.org/repositories/home:/ecsos/) repository, (OpenSuse Leap / Tumbleweed)

#### For Desktops not based on Qt, but on GTK i.e. Gnome, LxDE, XFCE etc, it may be neccessary to be installed, for proper appearance of icons, menus e.t.c., the following packages: 

- ##### `qt5ct` (All Distributions)
- ##### `qt5-style-plugins` (All Distributions, but OpenSuse)
- ##### `libqt5-qtstyleplugins-platformtheme-gtk2` (OpenSuSe) 
- ##### `qt5gtk2` may be needed for correct popup (baloons) messages
- ##### `gnome-shell-extension-topicons-plus` [AUR](https://aur.archlinux.org/packages/gnome-shell-extension-topicons-plus)

### For All Distributions, add `lsof` and `libnotify`, if are not installed at distribution's default installation. 

#### Select the name(s) from the above list, by the name of your Distribution (either major or derived from), respectively.

### Further Details at the [Wiki](https://gitlab.com/linuxer.gr/camera-control-webcam-switch-indicator/-/wikis/Project-History-and-other-Details)

#### For Project Releases, please add the [RSS Atom Feeds link](https://gitlab.com/psposito/camera-control-webcam-switch-indicator/-/tags?feed_token=hAsTBdxnMmMiYYCvGoHN&format=atom), to your RSS Feed Reader.
