#!/usr/bin/bash
export MYSECRET='sudopasswordtexxt'
package_type=$(command -v pacman || command -v apt || command -v yum)

distropack=${package_type:9}

if [[ ${package_type:9} == "apt" ]];
	then
	distro='Debian'
elif [[ ${package_type:9}  == "pacman" ]];
	then
	distro='Arch'
else
	distro='RPM'
fi

#echo $distropack
#echo $distro

session_type=$XDG_SESSION_TYPE

#QT_QPA_PLATFORM selection for xcb / wayland

if [[ ${session_type} -eq "x11" ]]; then
   QT_QPA_PLATFORM=xcb /opt/CameraControl/cameracontrol.bin -$distro >/dev/null
   exit 0
fi

if [[ ${session_type} -eq "wayland" ]]; then
   QT_QPA_PLATFORM=wayland /opt/CameraControl/cameracontrol.bin >/dev/null
   exit 0
fi
