#!/usr/bin/bash
#export MYSECRET='sudopasswordtext' has been depreciated, it uses polkit instead
gsettings set com.canonical.notify-osd gravity 2
package_type=$(command -v pacman || command -v apt || command -v yum || command -v xbps-fetch)
export $(dbus-launch)
distropack=${package_type:9}

if [[ ${package_type:9} == "apt" ]];
	then
	distro='Debian'
elif [[ ${package_type:9}  == "pacman" ]];
	then
	distro='Arch'
elif [[ ${package_type:5} == "xbps-fetch" ]];
	then
	distro='Void'
	distropack=${package_type:5}
else
	distro='RPM'
fi

#echo $distropack
#echo $distro

session_type=$XDG_SESSION_TYPE

#QT_QPA_PLATFORM selection for xcb / wayland

if [[ ${session_type} -eq "x11" ]]; then
   pkexec env DISPLAY=$DISPLAY XAUTHORITY=$XAUTHORITY QT_QPA_PLATFORM=xcb QT_STYLE_OVERRIDE=kvantum /opt/CameraControl/cameracontrol.bin -$distro >/dev/null
   exit 0
fi

if [[ ${session_type} -eq "wayland" ]]; then
   pkexec env DISPLAY=$DISPLAY XAUTHORITY=$XAUTHORITY QT_QPA_PLATFORM=xcb QT_STYLE_OVERRIDE=kvantum /opt/CameraControl/cameracontrol.bin -$distro >/dev/null
   exit 0
fi
